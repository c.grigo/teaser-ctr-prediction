import numpy as np
import scipy.special as spsp
from sklearn.preprocessing import OneHotEncoder
import tensorflow as tf


def scipy2tf_sparse(scipy_sparse_matrix):
    coo = scipy_sparse_matrix.tocoo()
    indices = np.mat([coo.row, coo.col]).transpose()
    return tf.SparseTensor(indices, coo.data, coo.shape)


def pd2tf_sparse(pandas_series, one_hot_encoder=None):
    quantity = np.expand_dims(pandas_series.values, 1)
    if one_hot_encoder is None:
        one_hot_encoder = OneHotEncoder().fit(quantity)
    return scipy2tf_sparse(one_hot_encoder.transform(quantity))


class LogisticRegressionModel(tf.keras.Model):
    def __init__(self, l2_weight_regularizer=None, l2_bias_regularizer=None, bias_ctr_initialization=.0015):
        super(LogisticRegressionModel, self).__init__()

        if l2_weight_regularizer:
            l2_weight_regularizer = tf.keras.regularizers.l2(l2_weight_regularizer)

        if l2_bias_regularizer:
            l2_bias_regularizer = tf.keras.regularizers.l2(l2_bias_regularizer)

        self.dense0 = tf.keras.layers.Dense(1, kernel_regularizer=l2_weight_regularizer,
                                            bias_regularizer=l2_bias_regularizer,
                                            bias_initializer=tf.keras.initializers.Constant(
                                                value=spsp.logit(bias_ctr_initialization)))

    def get_config(self):
        return {'name': 'logistic regression model'}

    def call(self, input_tensor):
        return self.dense0(input_tensor)

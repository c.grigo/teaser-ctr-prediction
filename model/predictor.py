import flask
import io
import numpy as np
import pandas as pd
import pickle
import sys
sys.path.extend(['/opt/program'])
import tensorflow as tf

from model import pd2tf_sparse


# predefined by sagemaker
LOCAL_MODEL_PATH = '/opt/ml/model'
DATA_PATH = '/opt/ml/input/data/'


class PredictionModel:
    """
    A singleton for holding the model. This simply loads the model and holds it. It has a predict function that does
    a prediction based on the model and the input data.
    """
    # Attributes where we keep the model when it's loaded
    model = None
    one_hot_encoders = None
    second_derivatives = None
    feature_cols = None
    n_predictive_samples = 1000
    noise_factor = 2.0

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.model is None:
            cls.model = tf.keras.models.load_model(LOCAL_MODEL_PATH)
            cls.one_hot_encoders = pickle.load(open(LOCAL_MODEL_PATH + '/one_hot_encoders.pkl', 'rb'))
            cls.second_derivatives = pickle.load(open(LOCAL_MODEL_PATH + '/second_derivatives.pkl', 'rb'))
            cls.feature_cols = cls.one_hot_encoders.keys()
        return

    @classmethod
    def encode_feature(cls, feature_col):
        """
        Takes a pandas Series of a categorical feature and one-hot encodes it to a ``tf.sparse.SparseTensor`` using
        the loaded one-hot encoders.

        :param feature_col: Series holding the categorical feature values.
        :type feature_col: pandas Series

        :return: Sparse design matrix of one-hot encoded features.
        :rtype: ``tf.sparse.SparseTensor``
        """
        return pd2tf_sparse(feature_col, cls.one_hot_encoders[feature_col.name])

    @classmethod
    def create_feature_matrix(cls, data):
        """
        One-hot encodes all features using the loaded one-hot encoders and concatenates the
        ``tf.sparse.SparseTensor`` design matrices to a single sparse feature matrix.

        :param data: DataFrame holding all the categorical features.
        :type data: pandas DataFrame

        :return: Sparse feature matrix.
        :rtype: ``tf.sparse.SparseTensor``
        """
        feature_encodings = []
        for feature in cls.feature_cols:
            feature_encodings.append(cls.encode_feature(data[feature]))
        return tf.sparse.concat(1, feature_encodings)

    @classmethod
    def probabilistic_prediction(cls, feature_matrix):
        """
        Determines the posterior over model parameters via Laplace approximation, then draws samples from this
        approximate posterior, assigns the sampled parameters to the model and calculates predictive samples to the
        input ``feature_matrix``. It calculates first and second moments of the predictions on-the-fly and uses them
        to determine a :math:`Beta` via moment matching which serves as the predictive distribution to the CTR.

        :param feature_matrix: Sparse, one-hot encoded feature matrix.
        :type feature_matrix: ``tf.sparse.SparseTensor``

        :return: :math:`Beta` predictive distribution parameters :math:`\\alpha` and :math:`\\beta`.
        :rtype: Tuple of ``np.array`` s
        """
        param_mean = {}
        for layer in cls.model.layers:
            param_mean[layer.kernel.name] = tf.squeeze(layer.kernel).numpy()
            param_mean[layer.bias.name] = layer.bias.numpy()

        param_std = {}
        for param in cls.second_derivatives:
            param_std[param] = 1.0/np.sqrt(cls.second_derivatives[param])

        param_samples = {}
        for param in param_mean:
            param_samples[param] = tf.random.normal([cls.n_predictive_samples, param_mean[param].shape[0]],
                                                    mean=param_mean[param], stddev=param_std[param])

        kernel_name = []
        bias_name = []
        for layer_index in range(len(cls.model.layers)):
            kernel_name.append(cls.model.layers[layer_index].kernel.name)
            bias_name.append(cls.model.layers[layer_index].bias.name)

        pred_mean = np.zeros((feature_matrix.shape[0], 1))
        pred_sq_mean = np.zeros((feature_matrix.shape[0], 1))
        for i in range(cls.n_predictive_samples):
            for layer_index in range(len(cls.model.layers)):
                cls.model.layers[layer_index].kernel.assign(
                    tf.expand_dims(tf.Variable(param_samples[kernel_name[layer_index]][i], dtype=tf.float32), 1))
                cls.model.layers[layer_index].bias.assign(
                    tf.Variable(param_samples[bias_name[layer_index]][i], dtype=tf.float32), 1)

            pred_sample = tf.math.sigmoid(cls.model(feature_matrix)).numpy()
            pred_mean = (i / (i + 1)) * pred_mean + pred_sample / (i + 1)
            pred_sq_mean = (i / (i + 1)) * pred_sq_mean + (pred_sample ** 2) / (i + 1)
        pred_var = cls.noise_factor**2 * (pred_sq_mean - pred_mean**2)

        factor1 = (pred_mean*(1.0 - pred_mean))/pred_var - 1.0
        alpha = pred_mean * factor1
        beta = (1.0 - pred_mean) * factor1

        # this is an artifact of moment matching when there's only few data
        alpha[alpha < .0] = .0
        beta[beta < .0] = .0

        return alpha, beta

    @classmethod
    def predict(cls, data):
        cls.get_model()
        feature_matrix = cls.create_feature_matrix(data)
        data['ctr_pred'] = tf.math.sigmoid(cls.model(feature_matrix)).numpy()
        data['teaser_ctr_alpha_pred'], data['teaser_ctr_beta_pred'] = cls.probabilistic_prediction(feature_matrix)
        return data


# The flask app for serving predictions
app = flask.Flask(__name__)


@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy. In this sample container, we declare it healthy if we can
    load the model successfully."""
    # health = ScoringService.get_model() is not None  # You can insert a health check here
    health = True

    status = 200 if health else 404
    return flask.Response(response='\n', status=status, mimetype='application/json')


@app.route('/invocations', methods=['POST'])
def transformation():
    """Do inference on a single batch of data. In this sample server, we take data as CSV, convert it to a pandas
    data frame for internal use and then convert the predictions back to CSV (which really just means one prediction
    per line, since there's a single column.
    """

    data = io.BytesIO(flask.request.data)
    data = pd.read_parquet(data)

    if len(data):
        # Do the prediction
        predictions = PredictionModel.predict(data)
    else:
        # if input dataframe is empty, return initial dataframe
        predictions = data

    out = io.BytesIO()
    predictions.to_parquet(out)
    result = out.getvalue()

    return flask.Response(response=result, status=200)

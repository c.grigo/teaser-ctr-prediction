#!/bin/sh

image=$1

docker run -v $(pwd)/testmodel:/opt/ml/model\
 -v $(pwd)/model/predictor.py:/opt/program/model/predictor.py\
  -p 8080:8080 --rm ${image} serve

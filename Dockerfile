FROM python:3.8

RUN apt-get update -y
RUN apt-get install -y --no-install-recommends nginx

# only for local testing, /opt/ml will be overwritten by sagemaker
COPY nginx.conf /opt/ml/nginx.conf
COPY hyperparameters.json /opt/ml/input/config/hyperparameters.json

COPY . /opt/program

RUN pip install --upgrade pip
RUN pip install -r /opt/program/requirements.txt

ENV PYTHONUNBUFFERED=TRUE
ENV PYTHONDONTWRITEBYTECODE=TRUE
ENV PATH="/opt/program/model:${PATH}"

WORKDIR /opt/program/model

#!/bin/sh

image=$1

docker run -v $(pwd)/publishing_articles_with_metrics.parquet:/opt/ml/input/data/train/publishing_articles_with_metrics.parquet\
           -v $(pwd)/unique_image_ids.parquet:/opt/ml/input/data/unique-image-ids/unique_image_ids.parquet\
           ${image} train
